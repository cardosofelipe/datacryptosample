import javax.crypto.Cipher;
import java.io.*;
import java.security.Key;
import java.security.KeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;


/**
 * RSA encryption/decryption utility class
 * Keys are supported in .der format
 */
public class CryptoUtils {
    private static Key publicKey = null;
    private static Key privateKey = null;


    public static void setPublicKey(String filePath) {
        File f = new File(filePath);
        byte[] keyBytes = getBytes(f);
        try {
            X509EncodedKeySpec spec =
                    new X509EncodedKeySpec(keyBytes);
            KeyFactory kf = KeyFactory.getInstance("RSA");

            publicKey = kf.generatePublic(spec);
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }


    public static void setPrivateKey(String filePath) {
        File f = new File(filePath);
        byte[] keyBytes = null;
        FileInputStream fis = null;
        keyBytes = getBytes(f);
        try {
            PKCS8EncodedKeySpec spec =
                    new PKCS8EncodedKeySpec(keyBytes);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            privateKey = kf.generatePrivate(spec);
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }



    public static String encryptToBase64(String toBeCiphred) {
        return encryptToBase64(publicKey, toBeCiphred);
    }

    public static String encryptToBase64(Key publicKey, String toBeCiphred) {
        try {
            byte[] ciphered = encrypt(publicKey, toBeCiphred.getBytes());

            return Base64.getEncoder().encodeToString(ciphered);
        } catch (Exception e) {
            System.err.println("Error while encrypting data: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }


    public static byte[] encrypt(Key publicKey, byte[] toBeCiphred) {
        try {
            Cipher rsaCipher = Cipher.getInstance("RSA");
            rsaCipher.init(Cipher.ENCRYPT_MODE, publicKey);
            return rsaCipher.doFinal(toBeCiphred);
        } catch (Exception e) {
            System.err.println("Error while encrypting data: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public static byte[] encrypt(byte[] toBeCiphred) {
        if (publicKey == null)
            try {
                throw new KeyException("Public key not initialized!");
            } catch (KeyException e) {
                e.printStackTrace();
            }
        return encrypt(publicKey, toBeCiphred);
    }


    public static String decryptFromBase64(String encryptedText) {
        return decryptFromBase64(privateKey, encryptedText);
    }

    public static String decryptFromBase64(Key privateKey, String encryptedText) {
        try {
//            Cipher rsaCipher = Cipher.getInstance("RSA");
//            rsaCipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] deciphered = decrypt(privateKey, Base64.getDecoder().decode(encryptedText));
            return stringify(deciphered);
        } catch (Exception e) {
            System.err.println("Error while decrypting data: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public static byte[] decrypt(byte[] encryptedData) {
        if (privateKey == null) {
            try {
                throw new KeyException("Private key not initialized!");
            } catch (KeyException e) {
                e.printStackTrace();
            }
        }
        return decrypt(privateKey, encryptedData);
    }

    public static byte[] decrypt(Key privateKey, byte[] encryptedData) {
        try {
            Cipher rsaCipher = Cipher.getInstance("RSA");
            rsaCipher.init(Cipher.DECRYPT_MODE, privateKey);
            return rsaCipher.doFinal(encryptedData);
        } catch (Exception e) {
            System.err.println("Error while decrypting data: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private static byte[] getBytes(File f) {
        byte[] keyBytes = null;
        FileInputStream fis;
        try {
            fis = new FileInputStream(f);
            DataInputStream dis = new DataInputStream(fis);
            keyBytes = new byte[(int) f.length()];
            dis.readFully(keyBytes);
            dis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return keyBytes;
    }

    public static String stringify(byte[] bytes) {
        return stringify(new String(bytes));
    }

    private static String stringify(String str) {
        String aux = "";
        for (int i = 0; i < str.length(); i++) {
            aux += str.charAt(i);
        }
        return aux;
    }


}
