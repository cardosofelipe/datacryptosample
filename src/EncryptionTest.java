
public class EncryptionTest {
    public static void main(String[] args) {

        CryptoUtils.setPublicKey("public_key.der");
        CryptoUtils.setPrivateKey("private_key.der");

        String sampleString = "Hello World";

        /** Begin String test **/

        System.out.println("Original String: " + sampleString);
        System.out.println("Encrypting with public.............\n");

        String encrypted = CryptoUtils.encryptToBase64(sampleString);

        System.out.println("Encrypted String: " + encrypted);
        System.out.println("Decrypting with private.............\n");

        String decrypted = CryptoUtils.decryptFromBase64(encrypted);

        System.out.println("Decrypted String: " + decrypted);


        System.out.println("\n******************************************************************************************************\n");


        /** Begin bytes test **/


        System.out.println("Original string: " + sampleString);
        System.out.println("Encrypting string bytes with public.............\n");

        byte[] encryptedBytes = CryptoUtils.encrypt(sampleString.getBytes());

        System.out.println("Encrypted string bytes: " + CryptoUtils.stringify(encryptedBytes));
        System.out.println("Decrypting bytes with private.............\n");

        byte[] decryptedBytes = CryptoUtils.decrypt(encryptedBytes);

        System.out.println("Decrypted string bytes: " + CryptoUtils.stringify(decryptedBytes));

    }
}
